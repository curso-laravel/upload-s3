<?php

namespace App\Services;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

/**
 * Description of ImageService
 *
 * @author Nataniel Paiva
 */
class ImageService
{

    public static function getPath()
    {
        return env('URL_CLOUDFRONT', 'http://d2313xwwyvrplw.cloudfront.net');
    }

    public static function resizeImage($file)
    {

        $img = Image::make($file);

        $img->resize(500, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        //detach method is the key! Hours to find it... :/
        $extension = $file->getClientOriginalExtension();
        $filePath = str_random(25) . "." . $extension;
        $resource = $img->stream()->detach();
        \Storage::disk('s3')->put(
            $filePath,
            $resource,
            'public'
        );
        return $filePath;
    }

    public static function uploadPicture(Request $request, $model, $id)
    {
        $picture = $request->file('picture');
        $extension = $picture->getClientOriginalExtension();
        $width = Image::make($picture)->width();
        $filesize = Image::make($picture)->filesize();

        if ($filesize > 500000 || $width > 599) {
            $filePath = self::resizeImage($request->file('picture'));
        } else {
            $filePath = str_random(25) . "." . $extension;
            $s3 = \Storage::disk('s3');
            $s3->put($filePath, file_get_contents($picture), 'public');
        }
        $oneModel = $model::find($id);
        $oneModel->picture_url = self::getPath() . $filePath;
        $oneModel->save();

    }


}
