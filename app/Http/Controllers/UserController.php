<?php
namespace App\Http\Controllers;

use App\Services\ImageService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index() {
        return response()->json(User::all(), 200);
    }

    public function store(Request $request){
        $user = new User();
        $user->fill($request->all());
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return response()->json($user, 200);
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        $user->fill($request->all());
        $user->save();
        return response()->json($user, 200);
    }

    public function destroy($id){
        User::destroy($id);
        return response()->json([], 200);
    }

    public function show($id){
        $user = User::find($id);
        return response()->json($user, 200);
    }

    public function uploadPicture(Request $request, $id) {

        $this->rules = ['picture' => 'required|mimes:jpeg,bmp,png'];
        $validator = Validator::make($request->file(), $this->rules);
        if ($validator->fails()) {
            return $validator->getMessageBag();
        }

        ImageService::uploadPicture($request, new User(), $id);
        return $this->show($id);
    }
}